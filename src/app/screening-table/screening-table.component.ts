import { Component, OnInit } from '@angular/core';
import { Screen } from '../models/screen';
import { screenings } from '../data/screening';
import { ScreeningService } from './screening.service';
import { Screening } from './screening';


@Component({
  selector: 'app-screening-table',
  templateUrl: './screening-table.component.html',
  styleUrls: ['./screening-table.component.css']
})
export class ScreeningTableComponent implements OnInit {

  
  constructor(private _screeningService: ScreeningService){}
  screenings: Screening[];

  getScreenings() :void {
    this._screeningService.getScreenings()
      .subscribe(
        screenings => (this.screenings = screenings)
      )
  }

/*  public getScreenings(): Array<Screen> {
    return screenings
      .map(s => new Screen(
        s.id,
        s.movieId,
        new Date(s.date),
        s.fillingOfHall,
        s.cinema,
        s.ticketPrice,
        s.travelTime
      )
    );
  }*/

  ngOnInit() {
  }

}
