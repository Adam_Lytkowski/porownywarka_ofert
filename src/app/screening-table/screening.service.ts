import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Screening } from './screening';
import { map } from 'rxjs/operators';

@Injectable()
export class ScreeningService {
  constructor(private http: HttpClient) { }

  configUrl = 'localhost:8080/test';

getScreenings() {
  return this.http
    .get<Screening[]>(this.configUrl)
    .pipe(map(data => data));
}
}