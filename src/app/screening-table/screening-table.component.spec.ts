import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreeningTableComponent } from './screening-table.component';

describe('ScreeningTableComponent', () => {
  let component: ScreeningTableComponent;
  let fixture: ComponentFixture<ScreeningTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreeningTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreeningTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
