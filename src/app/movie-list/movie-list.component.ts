import { Component, OnInit } from '@angular/core';
import { Movie } from "../data/movie";
import { movieList } from "../data/movie"

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent {

  public show = false;
  public movieIndex=0;

  mySlideOptions={
    margin: 5,
    
    dotsEach: true,
    responsive:{
      0:{
        items:1,
        nav:true
      },
      600:{
        items:3,
        nav:false
      },
      1000:{
        items:5,
        nav:true,
        loop:false
      }
    }
  };

  toggle(i){
    if(i==this.movieIndex){
    this.show = !this.show;
    this.movieIndex = i;
    }
    else{
      this.movieIndex = i;
    }
  }

  public getMovies(): Array<Movie> {
    return movieList
      .map(m => new Movie(
        m.id,
        m.title,
        m.description,
        m.dateOfPremiere,
        m.imgSrc
      )
    );
  }

  movieList = this.getMovies();

  
  
  

  constructor() {

   }

  ngOnInit() {
  }

}
