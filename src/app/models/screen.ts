export class Screen {

    constructor(
        public id: number,
        public movieId: number,
        public date: Date,
        public fillingOfHall: number,
        public cinema: string,
        public ticketPrice: number,
        public travelTime: string
    ){
    }
}