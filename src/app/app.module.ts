import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OwlModule } from 'ngx-owl-carousel';
import {  HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { ScreeningTableComponent } from './screening-table/screening-table.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieListComponent,
    ScreeningTableComponent
  ],
  imports: [
    BrowserModule,
    OwlModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
