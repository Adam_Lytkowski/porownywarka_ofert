export class Movie {

    constructor(
        public id: number,
        public title: string,
        public description: string,
        public dateOfPremiere: string,
        public imgSrc: string
    ){
    }
}

export var movieList: Array<Movie> = [
    {
     "id": 1,
    "title": "Bohemian Rhapsody",
    "description" : "Porywająca opowieść o zespole Queen, jego muzyce i niezwykłym wokaliście Freddie’em Mercurym (Rami Malek – serial TV ”Mr. Robot”), który przełamując stereotypy i konwencje, zdobył uwielbienie niezliczonych fanów. Film ukazuje błyskotliwą karierę zespołu, który dzięki ikonicznym utworom i rewolucyjnemu brzmieniu wspiął się na szczyty sławy, dopóki skandalizujący styl życia Mercury’ego nie postawił wszystkiego pod znakiem zapytania.",
    "dateOfPremiere" : "2018-11-02",
    "imgSrc" : "https://media.multikino.pl/thumbnails/50/rc/MUYzQUU1/eyJ0aHVtYm5haWwiOnsic2l6ZSI6WyIzMTkiLCI0NzciXSwibW9kZSI6Imluc2V0In19/uploads/images/films_and_events/bohemian-pl_1abefbedc2.jpg"
    },
    {
        "id": 2,
       "title": "Kler",
       "description" : "Wojtek Smarzowski, twórca wielokrotnie nagradzanych dzieł: „Wołyń”, „Pod Mocnym Aniołem”, „Drogówka”, „Róża”, „Dom zły” i „Wesele”, wraca z nowym filmem. „Kler” to obraz kościoła katolickiego jako instytucji oglądanej „od strony zakrystii”, rzucający światło na tematy niewygodne, trudne, częstokroć przemilczane, ale też poruszające.",
       "dateOfPremiere" : "2018-09-28",
       "imgSrc" : "https://media.multikino.pl/thumbnails/50/rc/Q0RBODBF/eyJ0aHVtYm5haWwiOnsic2l6ZSI6WyIzMTkiLCI0NzciXSwibW9kZSI6Imluc2V0In19/uploads/images/films_and_events/kler-pl_051aca5d95.jpg"
    },
    {
        "id": 3,
       "title": "Venom",
       "description" : "Tom Hardy jako Eddie Brock, tytułowy Venom – największy, najgroźniejszy przeciwnik Spider-Mana.",
       "dateOfPremiere" : "2018-10-05",
       "imgSrc" : "https://media.multikino.pl/thumbnails/50/rc/RTE1MkJG/eyJ0aHVtYm5haWwiOnsic2l6ZSI6WyIzMTkiLCI0NzciXSwibW9kZSI6Imluc2V0In19/uploads/images/films_and_events/venom-p_8add093460.jpg"
    },
    {
        "id": 4,
       "title": "Pierwszy człowiek",
       "description" : "Na fali wspólnego sukcesu sześciokrotnie nagrodzonego Oscarem® obrazu „La La Land” Damien Chazelle i Ryan Gosling ponownie łączą siły w historycznej opowieści o misji NASA, dzięki której na Księżycu wylądował człowiek. Film skupia się na życiu Neila Armstronga w latach 1961-1969.",
       "dateOfPremiere" : "2018-10-19",
       "imgSrc" : "https://media.multikino.pl/thumbnails/50/rc/QUNCMTdG/eyJ0aHVtYm5haWwiOnsic2l6ZSI6WyIzMTkiLCI0NzciXSwibW9kZSI6Imluc2V0In19/uploads/images/films_and_events/pierwszy-czlowiek-pl_831c43d937.jpg"
    },
    {
        "id": 5,
       "title": "Zwyczajna przysługa",
       "description" : "Stephanie jest uroczą, ale dość naiwną i niewinną młodą mamą, dla której szczytem występku i ryzykanctwa jest przejście przez ulicę na czerwonym świetle. Emily zaś to piękna, wyzwolona kobieta sukcesu, która trzyma życie za rogi.",
       "dateOfPremiere" : "2018-10-19",
       "imgSrc" : "https://media.multikino.pl/thumbnails/50/rc/QUU2QTdB/eyJ0aHVtYm5haWwiOnsic2l6ZSI6WyIzMTkiLCI0NzciXSwibW9kZSI6Imluc2V0In19/uploads/images/films_and_events/zwyczajna-przys-uga-pl_cfa5bf065b.jpg"
    },
    {
        "id": 6,
       "title": "Jak pies z kotem",
       "description" : "Najnowszy film Janusza Kondratiuka, autora niezapomnianych „Dziewczyn do wzięcia”, to słodko-gorzki obraz relacji rodzinnych, dla którego inspiracją jest prawdziwa historia.",
       "dateOfPremiere" : "2018-10-19",
       "imgSrc" : "https://media.multikino.pl/thumbnails/50/rc/N0MyOEE4/eyJ0aHVtYm5haWwiOnsic2l6ZSI6WyIzMTkiLCI0NzciXSwibW9kZSI6Imluc2V0In19/uploads/images/films_and_events/jak-pies-z-kotem-pl_daf82d66d6.jpg"
    },

    
]