export interface IScreen {
     id: number;
     movieId: number;
     date: string;
     fillingOfHall: number;
     cinema: string;
     ticketPrice: number;
     travelTime: string;
}

export var screenings: Array<IScreen> = [
{
    "id" : 1,
    "movieId" : 1,
    "date" : "2019-01-15T18:00:00",
    "fillingOfHall" : 0.35,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "25 minutes"
},
 {
    "id" : 2,
    "movieId" : 1,
    "date" : "2019-01-15T18:30:00",
    "fillingOfHall" : 0.25,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "20 minutes"
},
{
    "id" : 3,
    "movieId" : 1,
    "date" : "2019-01-15T19:00:00",
    "fillingOfHall" : 0.45,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "16 minutes"
},
{
    "id" : 4,
    "movieId" : 2,
    "date" : "2019-01-15T19:30:00",
    "fillingOfHall" : 0.35,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "25 minutes"
},
{
    "id" : 5,
    "movieId" : 2,
    "date" : "2019-01-15T20:00:00",
    "fillingOfHall" : 0.25,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "20 minutes"
},
{
    "id" : 6,
    "movieId" : 2,
    "date" : "2019-01-15T20:30:00",
    "fillingOfHall" : 0.45,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "16 minutes"
},
{
    "id" : 7,
    "movieId" : 3,
    "date" : "2019-01-15T21:00:00",
    "fillingOfHall" : 0.35,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "25 minutes"
},
{
    "id" : 8,
    "movieId" : 3,
    "date" : "2019-01-16T18:00:00",
    "fillingOfHall" : 0.25,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "20 minutes"
},
{
    "id" : 9,
    "movieId" : 3,
    "date" : "2019-01-16T18:30:00",
    "fillingOfHall" : 0.45,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "16 minutes"
},
{
    "id" : 10,
    "movieId" : 4,
    "date" : "2019-01-16T19:00:00",
    "fillingOfHall" : 0.35,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "25 minutes"
},
{
    "id" : 11,
    "movieId" : 4,
    "date" : "2019-01-16T19:30:00",
    "fillingOfHall" : 0.25,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "20 minutes"
},
{
    "id" : 12,
    "movieId" : 4,
    "date" : "2019-01-16T20:00:00",
    "fillingOfHall" : 0.45,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "16 minutes"
},
{
    "id" : 13,
    "movieId" : 5,
    "date" : "2019-01-16T20:30:00",
    "fillingOfHall" : 0.35,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "25 minutes"
},
{
    "id" : 14,
    "movieId" : 5,
    "date" : "2019-01-16T21:00:00",
    "fillingOfHall" : 0.25,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "20 minutes"
},
{
    "id" : 15,
    "movieId" : 5,
    "date" : "2019-01-17T18:00:00",
    "fillingOfHall" : 0.45,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "16 minutes"
},
{
    "id" : 16,
    "movieId" : 6,
    "date" : "2019-01-17T18:30:00",
    "fillingOfHall" : 0.35,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "25 minutes"
},
{
    "id" : 17,
    "movieId" : 6,
    "date" : "2019-01-17T19:00:00",
    "fillingOfHall" : 0.25,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "20 minutes"
},
{
    "id" : 18,
    "movieId" : 6,
    "date" : "2019-01-17T19:30:00",
    "fillingOfHall" : 0.45,
    "cinema" : "Multikino",
    "ticketPrice" : 15,
    "travelTime" : "16 minutes"
} 
]